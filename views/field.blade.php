<div class="{{ $class }}">
    {!! $label !!}

    {!! $element !!}

    @if ($hint)
        <div class="form-element-hint">{!! $hint !!}</div>
    @endif

    @if ($errors)
        <div class="error">{!! implode('<br>', $errors) !!}</div>
    @endif
</div>
