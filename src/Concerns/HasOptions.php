<?php

namespace Gsdk\Form\Concerns;

trait HasOptions
{
    private static array $defaultOptions = [];

    private static array $defaultMessages = [];

    private static array $elementsDefaults = [];

//    protected array $options = [];

    public static function setDefaults(array $options): void
    {
        self::$defaultOptions[static::class] = $options;
    }

    public static function setElementDefaults(array $options): void
    {
        self::$elementsDefaults[static::class] = $options;
    }

    public static function setDefaultMessages(array $messages): void
    {
        self::$defaultMessages[static::class] = $messages;
    }

    public function setOptions($options): static
    {
        foreach ($options as $k => $v) {
            $this->setOption($k, $v);
        }

        return $this;
    }

    public function setOption($key, $option): static
    {
        $this->options[$key] = $option;

        return $this;
    }

    public function getOption($key)
    {
        return $this->options[$key] ?? null;
    }

    public static function getDefaultElementOptions(): array
    {
        return array_merge([
            'required' => false,
            'nullable' => false,
            'disabled' => false,
            'readable' => true,
            'render' => true,
            'rules' => null, //validation rules
            'requiredText' => ''
        ], self::buildDefaults(self::$elementsDefaults));
    }

    protected function getDefaultMessages(): array
    {
        return self::buildDefaults(self::$defaultMessages);
    }

    protected function getDefaultOptions(): array
    {
        return array_merge($this->options, self::buildDefaults(self::$defaultOptions));
    }

    private static function buildDefaults(array $defaultsConfig): array
    {
        $defaults = [];

        foreach ($defaultsConfig as $class => $a) {
            if (is_subclass_of(static::class, $class)) {
                $defaults = array_merge($defaults, $a);
            }
        }

        if (isset($defaultsConfig[static::class])) {
            $defaults = array_merge($defaults, $defaultsConfig[static::class]);
        }

        return $defaults;
    }
}
