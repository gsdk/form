<?php

namespace Gsdk\Form\Element;

class Text extends Input
{
    protected array $options = [
        'inputType' => 'text',
        'trim' => true,
        'nullable' => true
    ];

    protected array $attributes = [
        'readonly',
        'required',
        'disabled',
        'autocomplete',
        'list',
        'maxlength',
        'minlength',
        'pattern',
        'placeholder',
        'size',
        'spellcheck'
    ];

    protected function prepareValue($value)
    {
        if (is_null($value)) {
            return $this->nullable ? $value : '';
        } elseif ($this->trim) {
            $value = trim(filter_var($value));
        }

        return empty($value) && $this->nullable ? null : $value;//, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW
    }
}
