<?php

namespace Gsdk\Form\Element;

class Range extends Input
{
    protected array $options = [
        'inputType' => 'range'
    ];
}
