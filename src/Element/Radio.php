<?php

namespace Gsdk\Form\Element;

use Gsdk\Form\Support\Radio\ItemBuilder;

class Radio extends AbstractElement
{
    protected array $options = [
        'groupClass' => 'radio-items',
        'itemClass' => 'radio-item',
        'inputClass' => '',
        'labelClass' => '',
        'valueIndex' => 'id',
        'textIndex' => 'name',
        'multiple' => false,
        'inline' => true,
        'disabled' => false,
    ];

    private array $items = [];

    public function __construct(string $name, array $options = [])
    {
        if (isset($options['items'])) {
            $this->setItems($options['items']);
            unset($options['items']);
        } elseif (isset($options['enum'])) {
            $this->items = ItemBuilder::fromEnum($options['enum']);
        }

        parent::__construct($name, $options);
    }

    public function setItems(iterable $items): void
    {
        foreach ($items as $k => $data) {
            $this->items[] = (new ItemBuilder($k, $data))
                ->setValue($this->valueIndex)
                ->setText($this->textIndex)
                ->get();
        }
    }

    public function isChecked($value): bool
    {
        $selected = $this->getValue();

        if (!$this->multiple) {
            return ($value == $selected);
        } elseif (is_array($selected)) {
            return in_array($value, $selected);
        } else {
            return false;
        }
    }

    public function valueExists($value): bool
    {
        foreach ($this->items as $item) {
            if ($item->value === $value) {
                return true;
            }
        }

        return false;
    }

    protected function prepareValue($value)
    {
        if ($this->multiple) {
            if (!is_iterable($value)) {
                return [];
            }

            $values = [];
            foreach ($value as $val) {
                if (is_object($val)) {
                    $val = $val->id;
                }

                if ($this->valueExists($val)) {
                    $values[] = $val;
                }
            }

            return $values;
        } else {
            return $value;
        }
    }

    public function getHtml(): string
    {
        $html = '<div class="' . $this->groupClass . '">';
        if ($this->multiple) {
            $inputName = $this->getInputName() . '[]';
            $inputType = 'checkbox';
        } else {
            $inputName = $this->getInputName();
            $inputType = 'radio';
        }
        foreach ($this->items as $i => $item) {
            $html .= $this->getInputHtml($inputType, $item, $inputName, $i === 0);
        }

        $html .= '</div>';

        return $html;
    }

    private function getInputHtml(string $type, object $item, string $inputName, bool $isFirst): string
    {
        $inputId = $this->getInputId() . '_' . $item->value;

        return '<div class="' . $this->itemClass . '">'
            . '<input type="' . $type . '"'
            . ' class="' . $this->inputClass . '"'
            . ' value="' . htmlspecialchars($item->value) . '"'
            . ' name="' . $inputName . '"'
            . ' id="' . $inputId . '"'
            . ($isFirst && $this->isRequired() ? ' required' : '')
            . ($this->options['disabled'] ? ' disabled' : '')
            . ($this->isChecked($item->value) ? ' checked' : '') . '>'
            . '<label class="' . $this->labelClass . '" for="' . $inputId . '">' . $item->text . '</label>'
            . '</div>';
    }
}
