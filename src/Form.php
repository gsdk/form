<?php

namespace Gsdk\Form;

use Illuminate\Support\Facades\Facade;

/**
 * @method static FormBuilder name(string $name)
 * @method static FormBuilder method(string $method)
 * @method static FormBuilder action(string $action)
 * @method static FormBuilder checkbox(string $name, array $options = [])
 * @method static FormBuilder date(string $name, array $options = [])
 * @method static FormBuilder select(string $name, array $options = [])
 * @method static FormBuilder hidden(string $name, array $options = [])
 * @method static FormBuilder text(string $name, array $options = [])
 * @method static FormBuilder textarea(string $name, array $options = [])
 * @method static FormBuilder email(string $name, array $options = [])
 * @method static FormBuilder phone(string $name, array $options = [])
 * @method static FormBuilder file(string $name, array $options = [])
 * @method static FormBuilder image(string $name, array $options = [])
 * @method static FormBuilder color(string $name, array $options = [])
 * @method static FormBuilder password(string $name, array $options = [])
 * @method static FormBuilder number(string $name, array $options = [])
 * @method static FormBuilder url(string $name, array $options = [])
 */
class Form extends Facade
{
    protected static $cached = false;

    protected static function getFacadeAccessor()
    {
        return FormBuilder::class;
    }
}
