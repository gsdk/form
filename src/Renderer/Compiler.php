<?php

namespace Gsdk\Form\Renderer;

use Illuminate\Contracts\View\Factory as ViewFactory;

class Compiler
{
    public static function compile(string $template, array $data): string
    {
        $factory = app(ViewFactory::class);
        $view = realpath(__DIR__ . '/../../views/' . $template . '.blade.php');

        return (string)$factory->file($view, $data);
    }
}
