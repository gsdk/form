<?php

namespace Gsdk\Form\Support\SelectBox;

use Gsdk\Form\Support\Radio\ItemBuilder;

class OptionBuilder extends ItemBuilder
{
    private static array $groupKeys = ['parent_id', 'group_id'];

    private static array $parentKeys = ['parent_id'];

    public function setGroup(?string $key): static
    {
        $this->option->groupId = $this->find($key, 'groupId', self::$groupKeys);

        return $this;
    }

    public function setParent(?string $key): static
    {
        $this->option->parentId = $this->find($key, 'parentId', self::$parentKeys);

        return $this;
    }
}
